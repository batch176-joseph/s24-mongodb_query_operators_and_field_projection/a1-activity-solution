


db.users.find(
	{$or:
		[
		{firstName:
 				{$regex:'y',$options:`$i`}
		},
		{lastName:
 				{$regex:'y',$options:`$i`}
		}
		]
	},
	{
		"email":1, "isAdmin":1, "_id":0
	}
)


db.users.find(
	{$and:
		[
		{firstName:
 				{$regex:'y',$options:`$i`}
		},
		{isAdmin:true}
		]
	},
	{
		"email":1, "isAdmin":1, "_id":0
	}
)


db.products.find(
	{$and:
		[
		{name:
 				{$regex:'x',$options:`$i`}
		},
		{price:
			{$gte:50000}
		}
		]
	}
)


db.products.updateMany(
	{
		price: {$lt:2000}
	},
	{
		$set: {
			"isActive": false
		}
	}
)


db.products.deleteMany(
	{
		price: {$gt:20000}
	}
)